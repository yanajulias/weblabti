-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 03 Apr 2019 pada 21.03
-- Versi Server: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pjlabti`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `penanggungjawab`
--

CREATE TABLE `penanggungjawab` (
  `shift` varchar(1) NOT NULL,
  `waktu` varchar(11) NOT NULL,
  `hari` varchar(6) NOT NULL,
  `kelas` varchar(5) NOT NULL,
  `matprak` varchar(4) NOT NULL,
  `nama` varchar(20) NOT NULL,
  `asisten` varchar(20) NOT NULL,
  `ruang` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `penanggungjawab`
--

INSERT INTO `penanggungjawab` (`shift`, `waktu`, `hari`, `kelas`, `matprak`, `nama`, `asisten`, `ruang`) VALUES
('3', '12.00-14.00', 'Rabu', '1IA01', 'AP2', 'Farhan Taufik', 'Aditya Arya', 'H406'),
('2', '10.00-12.00', 'Rabu', '1IA02', 'AP2', 'Bryantama', '', 'H406'),
('1', '08.00-10.00', 'Rabu', '1IA03', 'AP2', 'Wisnundari', 'Tiara Sukma', 'H406'),
('1', '08.00-10.00', 'Kamis', '1IA04', 'AP2', 'Marisi Muhammad', 'Arya Pasha', 'H406'),
('1', '08.00-10.00', 'Jumat', '1IA05', 'AP2', 'Desi Fibrianti', 'Dimas Agung', 'H406'),
('1', '08.00-10.00', 'Senin', '1IA06', 'AP2', 'Siti Anggraini', 'Bella Sabrina', 'H406'),
('5', '16.00-18.00', 'Kamis', '1IA07', 'AP2', 'Ary Roosyid', 'Muhammad Boby', 'H406'),
('2', '12.00-14.00', 'Kamis', '1IA08', 'AP2', 'Melinda Putri', '', 'H406'),
('4', '14.00-16.00', 'Kamis', '1IA09', 'AP2', 'Ary Roosyid', 'Luthfriandi', 'H406'),
('3', '12.00-14.00', 'Kamis', '1IA10', 'AP2', 'Luthfriandi', 'Ary Roosyid', 'H406'),
('3', '14.00-12.00', 'Selasa', '1IA11', 'AP2', 'Riqi Ahmad', 'Aristiawan Wiguna', 'H406'),
('5', '16.00-18.00', 'Selasa', '1IA12', 'AP2', 'Riandra', 'Geadalfa', 'H406'),
('1', '08.00-10.00', 'Selasa', '1IA13', 'AP2', 'Reza Fanani', 'Vincen Alvin', 'H406'),
('4', '14.00-16.00', 'Selasa', '1IA14', 'AP2', 'Marisi Muhammad', 'Maria Gabriela', 'H406'),
('1', '08.00-10.00', 'Sabtu', '1IA15', 'AP2', 'Eka Setianingsih', 'Adham Dhava', 'H406'),
('4', '14.00-16.00', 'Jumat', '1IA16', 'AP2', 'Mustopah', 'Ananda Phylia', 'H406'),
('5', '16.00-18.00', 'Jumat', '1IA17', 'AP2', 'Ayi Imani', 'Dania Noorafan', 'H406'),
('1', '07.30-09.30', 'Sabtu', '3IA01', 'GK2', 'Muhammad Aflisal', 'Muhammad Yunus', 'E532'),
('3', '11.30-13.30', 'Selasa', '3IA02', 'GK2', 'Fery Pratama', 'Dita Ruth', 'E532'),
('2', '09.30-11.30', 'Jumat', '3IA03', 'GK2', 'Hayatul Fikri', 'Dionisius', 'E532'),
('5', '15.30-17.30', 'Rabu', '3IA04', 'GK2', 'Ilham Akbar', 'Jesika Ansella', 'E531'),
('3', '11.30-13.30', 'Sabtu', '3IA05', 'GK2', 'Figa Rizfa', 'E532', ''),
('2', '09.30-12.30', 'Kamis', '3IA06', 'GK2', 'Wisnundari', 'Sigit Ari', 'E531'),
('1', '07.30-09.30', 'Jumat', '3IA07', 'GK2', 'Putu Bagus', 'Dionisius', 'E532'),
('5', '15.30-17.30', 'Kamis', '3IA08', 'GK2', 'Iqbal Mahfudzi', ' ', 'E532'),
('4', '13.30-15.30', 'Jumat', '3IA09', 'GK2', 'Soni Fauzan', 'Dionisius', 'E532'),
('5', '15.30-17.30', 'Selasa', '3IA10', 'GK2', 'Muhammad Geraldhia', 'Arif Bagus', 'E531'),
('4', '13.30-15.30', 'Selasa', '3IA11', 'GK2', 'Dzaky Rafid', 'Arif Bagus', 'E531'),
('5', '15.30-17.30', 'Kamis', '3IA12', 'GK2', 'Aristiawan Wiguna', 'Maulana Iskandar', 'E531'),
('5', '15.30-17.30', 'Jumat', '3IA13', 'GK2', 'Arif Bagus', 'Ary Roosyid', 'E532'),
('1', '07.30-08.30', 'Kamis', '3IA14', 'GK2', 'Alam Arthansyah', 'Sigit Ari', 'E531'),
('2', '09.30-11.30', 'Kamis', '3IA15', 'GK2', 'Muhammad Guruh', 'Muhammad Yunus', 'E532'),
('1', '07.30-09.30', 'Kamis', '3IA16', 'GK2', 'Dzaky Rafid', 'Ardea Bagas', 'E531'),
('4', '15.30-17.30', 'Kamis', '3IA17', 'GK2', 'Delta El Kamal', 'Anita', 'E532'),
('3', '12-00-14.00', 'Selasa', '4IA01', 'RPL2', 'Pranadipa', 'Bayu Yunan', 'H402'),
('1', '08.00-10.00', 'Sabtu', '4IA02', 'RPL2', 'Adrian', 'Putu Bagus', 'H402'),
('2', '10.00-12.00', 'Sabtu', '4IA03', 'RPL2', 'Adrian', 'Maulana Iskandar', 'H402'),
('1', '08.00-10.00', 'Jumat', '4IA04', 'RPL2', 'Pranadipa', 'Fahrul Andrian', 'H402'),
('4', '14.00-16.00', 'Selasa', '4IA05', 'RPL2', 'Dionisius', 'Muhammad Geraldhia', 'H402'),
('5', '16.00-18.00', 'Jumat', '4IA06', 'RPL2', 'Tamimah Z', 'Nabilla Nurfadliana', 'H402'),
('3', '12.00-14.00', 'Sabtu', '4IA07', 'RPL2', 'Giventris Jeremia', 'Sigit Ari', 'H402'),
('1', '08.00-12.00', 'Selasa', '4IA08', 'RPL2', 'Ramdhany Satria', 'Amalia Mandarwati', 'H402'),
('5', '16.00-18.00', 'Jumat', '4IA09', 'RPL2', 'Anita', 'Bobby Rikinaldo', 'H402'),
('4', '14.00-16.00', 'Senin', '4IA10', 'RPL2', 'Yana Julia', 'Jesika Ansella', 'H402'),
('2', '10.00-12.00', 'Selasa', '4IA11', 'RPL2', 'Muhammad Geraldhia', 'Amalia Mandarwati', 'H402'),
('1', '08.00-10.00', 'Kamis', '4IA12', 'RPL2', 'Suci Hayati', 'Erra Anggi', 'H402'),
('2', '10.00-12.00', 'Kamis', '4IA13', 'RPL2', 'Anita', 'Ramdhany Satria', 'H402'),
('3', '12.00-14.00', 'Kamis', '4IA14', 'RPL2', 'Qais Cahyo', 'Erra Anggi', 'H402'),
('1', '08.00-10.00', 'Senin', '4IA15', 'RPL2', 'Pranadipa', 'Muhammad Naufaldi', 'H402'),
('5', '16.00-18.00', 'Senin', '4IA16', 'RPL2', 'Jesika Ansella', 'Yana Julia', 'H402'),
('5', '16.00-18.00', 'Selasa', '4IA17', 'RPL2', 'Fahrul Andrian', 'Muhammad Geraldhia', 'H402'),
('3', '12.00-14.00', 'Rabu', '3IA01', 'SBD2', 'Erra Anggi', 'Giventris Jeremia', 'H4045'),
('3', '12.00-14.00', 'Kamis', '3IA02', 'SBD2', 'Suci Hayati', 'Alam Arthansyah', 'H4045'),
('5', '16.00-18.00', 'Jumat', '3IA03', 'SBD2', 'Fajar Satriatna', 'Marisi Muhammad', 'H4045'),
('3', '12.00-14.00', 'Rabu', '3IA04', 'SBD2', 'Muhammad Naufaldi', 'Alam Arthansyah', 'H4045'),
('1', '08.00-10.00', 'Kamis', '3IA05', 'SBD2', 'Jasmine Athira', 'Mustopah', 'H4045'),
('4', '14.00-16.00', 'Selasa', '3IA06', 'SBD2', 'Nur Syaffia', 'Bayu Yunan', 'H4045'),
('4', '14.00-16.00', 'Jumat', '3IA07', 'SBD2', 'Sigit Ari', 'Batari Wahyu', 'H4045'),
('2', '08.00-10.00', 'Sabtu', '3IA08', 'SBD2', 'Rafif Favian', 'Hayatul Fikri', 'H4045'),
('5', '16.00-18.00', 'Senin', '3IA09', 'SBD2', 'Sarah Nur', 'Ayi Imani', 'H4045'),
('1', '08.00-10.00', 'Sabtu', '3IA10', 'SBD2', 'Farisha Irwayu', 'Reza Fanani', 'H4045'),
('2', '10.00-12.00', 'Senin', '3IA11', 'SBD2', 'Eunike Kamase', 'Siti Anggraini', 'H4045'),
('1', '08.00-10.00', 'Rabu', '3IA12', 'SBD2', 'Nabila Nurfadliana', 'Hendar Alfiyanto', 'H4045'),
('5', '16.00-18.00', 'Jumat', '3IA13', 'SBD2', 'Muhammad Reza', 'Farhan Taufik', 'H4045'),
('1', '08.00-10.00', 'Jumat', '3IA14', 'SBD2', 'Batari Wahyu', 'Mustopah', 'H4045'),
('1', '08.00-10.00', 'Senin', '3IA15', 'SBD2', 'Maulana Iskandar', 'Eunike Kamase', 'H4045'),
('4', '14.00-16.00', 'Senin', '3IA16', 'SBD2', 'Dania Noorafan', 'Anandra Phylia', 'H4045'),
('2', '08.00-10.00', 'Kamis', '3IA17', 'SBD2', 'Jasmine Athira', 'Putu Bagus', 'H4045');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
