<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class  Data_model extends CI_Model {

	function caripj()
	{
		return $this->db->get('penanggungjawab');
	}

	function hasilcari($katakunci)
	{
		$this->db->like('kelas',$katakunci);
		$this->db->or_like('matprak',$katakunci);
		$this->db->or_like('nama',$katakunci);
		$this->db->or_like('asisten',$katakunci);
		$this->db->or_like('ruang',$katakunci);
		return $this->db->get('penanggungjawab');
	}

}