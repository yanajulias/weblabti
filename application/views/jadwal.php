<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

	<!DOCTYPE html>
	<html lang="zxx" class="no-js">

		<body>
			<header id="header" id="home">
		    <div class="container">
		    	<div class="row align-items-center justify-content-between d-flex">
				    <div id="logo">
				      <a href="<?php echo site_url();?>"><img src="<?php echo base_url('asset/img/logo-labti.png'); ?>" width="50px"/></a>
				    </div>
			      <nav id="nav-menu-container">
			        <ul class="nav-menu align-items-center justify-content-between d-flex">
			          <li><a href="<?php echo site_url();?>">Beranda</a></li>
				        <!-- <li class="menu-has-children"><a href="#">Praktikum</a>
				          <ul>
				         		<li><a href="">Tata Tertib</a></li>
				 	          <li><a href="#">Jadwal</a></li>
					          <li><a href="#">Modul</a></li>
				          	<li><a href="#">Pelayanan</a></li>
				          </ul>
				        </li> -->
<!-- 				    		<li><a href="#">Berita</a></li> -->
				        <li><a href="<?php echo site_url('tatatertib');?>">Tata Tertib</a></li>
				        <li class="menu-active"><a href="<?php echo site_url('jadwal');?>">Jadwal</a></li>
				        <li><a href="#">Masuk</a></li>
			        </ul>
			      </nav><!-- #nav-menu-container -->		    		
		    	</div>
		    </div>
			</header><!-- #header -->

			<!-- Start Content Area -->
			<section class="sample-text-area">
				<div class="container">
					<div class="text-center">
						<h1>Jadwal Praktikum</h1>
						<h3 class="text-heading">Laboratorium Teknik Informatika</h3>
						  <header class="masthead text-white text-center">
					      <div class="overlay"></div>
					      <div class="container">
					        <div class="row">
					          <!-- <div class="col-xl-9 mx-auto">
					            <h1 class="mb-5">Apa yang ingin kamu cari?</h1>
					          </div> -->
					          <div class="col-md-10 col-lg-8 col-xl-7 mx-auto">
					            <form action="<?php echo base_url('welcome/mencari');?>" method="post">
					              <div class="form-row">
					                <div class="col-12 col-md-9 mb-2 mb-md-0">
					                  <input type="text" name ="katakunci" class="form-control form-control-lg" placeholder="Input kelas/nama penanggung jawab">
					                </div>
					                <div class="col-12 col-md-2">
					                  <button class="primary-btn mt-20 text-uppercase">Cari!<span class="lnr lnr-arrow-right"></button>
					                </div>
					              </div>
					            </form>
					          </div>
					        </div>
					      </div>
					    </header>

 	   				
					<div class="">
						<img class="img-fluid col-lg-12" src="https://ti-dasar.lab.gunadarma.ac.id/wp-content/uploads/2019/03/Screen-Shot-2019-03-16-at-13.50.02.png">
						<img class="img-fluid col-lg-12" src="https://ti-dasar.lab.gunadarma.ac.id/wp-content/uploads/2019/03/Screen-Shot-2019-03-16-at-13.50.16.png">
						<img class="img-fluid col-lg-12" src="https://ti-dasar.lab.gunadarma.ac.id/wp-content/uploads/2019/03/Screen-Shot-2019-03-16-at-13.50.28.png">
						<img class="img-fluid col-lg-12" src="https://ti-dasar.lab.gunadarma.ac.id/wp-content/uploads/2019/03/Screen-Shot-2019-03-16-at-13.50.36.png">
						<img class="img-fluid col-lg-12" src="https://ti-dasar.lab.gunadarma.ac.id/wp-content/uploads/2019/03/Screen-Shot-2019-03-16-at-22.48.23.png">
						<img class="img-fluid col-lg-12" src="https://ti-dasar.lab.gunadarma.ac.id/wp-content/uploads/2019/03/Screen-Shot-2019-03-16-at-13.50.59.png">
					</div>
				</div>
			</section>
			<!-- End Content Area -->
