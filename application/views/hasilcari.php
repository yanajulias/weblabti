<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

	<!DOCTYPE html>
	<html lang="zxx" class="no-js">

		<body>
			<header id="header" id="home">
		    <div class="container">
		    	<div class="row align-items-center justify-content-between d-flex">
				    <div id="logo">
				      <a href="<?php echo site_url();?>"><img src="<?php echo base_url('asset/img/logo-labti.png'); ?>" width="50px"/></a>
				    </div>
			      <nav id="nav-menu-container">
			        <ul class="nav-menu align-items-center justify-content-between d-flex">
			          <li><a href="<?php echo site_url();?>">Beranda</a></li>
				        <!-- <li class="menu-has-children"><a href="#">Praktikum</a>
				          <ul>
				         		<li><a href="">Tata Tertib</a></li>
				 	          <li><a href="#">Jadwal</a></li>
					          <li><a href="#">Modul</a></li>
				          	<li><a href="#">Pelayanan</a></li>
				          </ul>
				        </li> -->
<!-- 				    		<li><a href="#">Berita</a></li> -->
				        <li><a href="<?php echo site_url('tatatertib');?>">Tata Tertib</a></li>
				        <li class="menu-active"><a href="<?php echo site_url('jadwal');?>">Jadwal</a></li>
				        <li><a href="#">Masuk</a></li>
			        </ul>
			      </nav><!-- #nav-menu-container -->		    		
		    	</div>
		    </div>
			</header><!-- #header -->

			<!-- Start Content Area -->
			<section class="sample-text-area">
				<div class="container">
					<div class="text-center">
						<h1>Jadwal Praktikum</h1>
						<h3 class="text-heading">Laboratorium Teknik Informatika</h3>
						  <!--   <header class="masthead text-white text-center">
     							 <div class="overlay"></div>
      								<div class="container">
       									 <div class="row">
          									<!-- <div class="col-xl-9 mx-auto">
           										 <h1 class="mb-5">Apa yang ingin kamu cari?</h1>
         									 </div> -->
         								 <!-- <div class="col-md-10 col-lg-8 col-xl-7 mx-auto">
            						<form action="<?php echo base_url('welcome/caripj/');?>" method="post">
              							<div class="form-row">
                							<div class="col-12 col-md-9 mb-2 mb-md-0">
                  								<input type="text" name ="katakunci" class="form-control form-control-lg" placeholder="Masukkan kelas...">
               								 </div>
                 							<div class="col-12 col-md-3">
                  								<button type="submit" class="btn btn-block btn-lg btn-primary">Cari!</button>
                							</div>
              							</div>
            						</form>
         						 </div>
        					</div>
      					</div>
    				</header> --> 

    				
					<section class="sample-text-area">
						<div class="container">
							<div class="text-center">
								<!-- <h1>Jadwal Praktikum</h1> -->
								<!-- <h3 class="text-heading">Laboratorium Teknik Informatika</h3> -->
								  <header class="masthead text-white text-center">
							      <div class="overlay"></div>
							      <div class="container">
							        <div class="row">
							          <<!-- div class="col-xl-9 mx-auto">
							            <h1 class="mb-5">Apa yang ingin kamu cari?</h1>
							          </div> -->
							          <div class="col-md-10 col-lg-8 col-xl-7 mx-auto">
							            <form action="<?php echo base_url('welcome/mencari');?>" method="post">
							              <div class="form-row">
							                <div class="col-12 col-md-9 mb-2 mb-md-0">
							                  <input type="text" name ="katakunci" class="form-control form-control-lg" placeholder="Masukkan kata kunci...">
							                  
							                </div>
							                <div class="col-12 col-md-3">
							                	<button class="primary-btn mt-20 text-uppercase">Cari!<span class="lnr lnr-arrow-right"></button>
							                  
							                </div>
							              </div>
							            </form>
							          </div>
							        </div>
							      </div>
							    </header>


					<table class="table table-striped" align="center">
					    <thead>
					      <tr align="center">
					        <th rowspan="1">Shift</th>
					        <th rowspan="1">Waktu</th>
					        <th rowspan="1">Hari</th>
					        <th rowspan="1">Kelas</th>
					        <th rowspan="1">Matprak</th>
					        <th rowspan="1">Penanggung Jawab</th>
					        <th rowspan="1">Asisten</th>
					        <th rowspan="1">Ruang</th>
					      </tr>
					    </thead>

					      <tbody>
					    <?php foreach($penanggungjawab->result() as $row): ?>
					      <tr align="center">
					        <td> <?php echo $row->shift; ?></td>
					        <td> <?php echo $row->waktu; ?></td>
					        <td> <?php echo $row->hari; ?></td>
					        <td> <?php echo $row->kelas; ?></td>
					        <td> <?php echo $row->matprak; ?></td>
					        <td> <?php echo $row->nama; ?></td>
					        <td> <?php echo $row->asisten; ?></td>
					        <td> <?php echo $row->ruang; ?></td>
					      </tr>
					    <?php endforeach; ?>
					    </tbody>

					  </table>
			</section>

			<!-- End Content Area -->