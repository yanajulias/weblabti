<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {


	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('Data_model');
	}

	public function index()
	{
		$this->load->view('include/header');
		$this->load->view('index');
		$this->load->view('include/footer');
	}

	public function tatatertib()
	{
		$this->load->view('include/header');
		$this->load->view('tatatertib');
		$this->load->view('include/footer');
	}

	public function jadwal()
	{
		$this->load->view('include/header');
		$this->load->view('jadwal');
		$this->load->view('include/footer');
	}

	public function profil()
	{
		$this->load->view('include/header');
		$this->load->view('profil');
		$this->load->view('include/footer');
	}

	public function hasilcari()
	{
		$this->load->view('include/header');
		$this->load->view('hasilcari');
		$this->load->view('include/footer');
	}

	public function table()
	{
		$data['penanggungjawab']=$this->Data_model->caripj();
		$this->load->view('hasilcari.php', $data);
	}

	public function mencari()
	{
		$katakunci = $this->input->post('katakunci');
		$data['penanggungjawab']=$this->Data_model->hasilcari($katakunci);
		$this->load->view('hasilcari',$data);
		$this->load->view('include/header');

		$this->load->view('include/footer');
		
	}	

}
